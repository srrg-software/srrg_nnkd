#pragma once
#include "neural_network_caffe.h"

namespace nnkd {

//! @class neural network based keypoint detector
class KeypointDetector {

//ds object handling
public:

  //! @brief the stereo camera setup must be provided
  KeypointDetector();

  //! @brief to be called after constructor and parameters are set
  //! @param[in] proto_file_
  //! @param[in] model_file_
  //! @param[in] gpu_mode_
  //! @param[in] number_of_rows_image_
  //! @param[in] number_of_cols_image_
  //! @param[in] sliding_window_step_size_
  void configure(const std::string& proto_file_,
                 const std::string& model_file_,
                 const bool& gpu_mode_,
                 const uint32_t& number_of_rows_image_,
                 const uint32_t& number_of_cols_image_,
                 const uint32_t& sliding_window_step_size_ = 10);

  //! @brief cleanup of dynamic structures
  ~KeypointDetector();

//ds functionality
public:

  //! @brief detects keypoints and stores them in an opencv::KeyPoint vector
  //! @param[in] intensity_image_ the image in which keypoints are to be detected
  //! @param[out] keypoints_ detected keypoints in the provided image
  void detect(const cv::Mat& intensity_image_, std::vector<cv::KeyPoint>& keypoints_);

  //! @brief detects keypoints (in batch) and stores them in an opencv::KeyPoint vector
  //! @param[in] intensity_image_ the image in which keypoints are to be detected
  //! @param[out] keypoints_ detected keypoints in the provided image
  void detectBatch(const cv::Mat& intensity_image_, std::vector<cv::KeyPoint>& keypoints_);

  
//ds getters/setters
public:

  //! @brief sets the number of rows in an image (height in pixels)
  //! @param[in] number_of_rows_image_ number of pixel rows
  void setNumberOfRowsImage(const uint32_t& number_of_rows_image_) {number_of_rows_image = number_of_rows_image_;}

  //! @brief sets the number of cols in an image (width in pixels)
  //! @param[in] number_of_cols_image_ number of pixel cols
  void setNumberOfColsImage(const uint32_t& number_of_cols_image_) {number_of_cols_image = number_of_cols_image_;}

  //! @brief adjusts the detection sliding window granularity
  //! @param[in] sliding_window_step_size_ number of pixels
  void setSlidingWindowStepSize(const uint32_t& sliding_window_step_size_) {sliding_window_step_size = sliding_window_step_size_;}

//ds settings
protected:

  //! @brief input properties
  uint32_t number_of_rows_image = 0;
  uint32_t number_of_cols_image = 0;

  //! @brief point detection properties
  uint32_t target_number_of_keypoints = 1000;
  const float keypoint_size_pixels    = 7;

  //! @brief sliding window configuration for patch detection
  const uint32_t sliding_window_rows     = 30;
  const uint32_t sliding_window_cols     = 30;
  const uint32_t sliding_window_row_init = 15;
  const uint32_t sliding_window_col_init = 15;
  uint32_t sliding_window_step_size      = 10;

private:

  //! @brief neural network instance
  NeuralNetworkCaffe* _network = 0;
};
}
