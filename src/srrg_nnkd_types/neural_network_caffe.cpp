#include "../srrg_nnkd_types/neural_network_caffe.h"

namespace nnkd {

void NeuralNetworkCaffe::wrapInputLayer(std::vector<cv::Mat>& input_channels_) {
  caffe::Blob<float> *input_layer = _gl_network->input_blobs()[0];

  int width = input_layer->width();
  int height = input_layer->height();
  float *input_data = input_layer->mutable_cpu_data();
  for (int i = 0; i < input_layer->channels(); ++i) {
    cv::Mat channel(height, width, CV_32FC1, input_data);
    input_channels_.push_back(channel);
    input_data += width * height;
  }
}

void NeuralNetworkCaffe::wrapBatchInputLayer(std::vector<std::vector<cv::Mat> > &input_batch_) {
  caffe::Blob<float> *input_layer = _gl_network->input_blobs()[0];

  int width = input_layer->width();
  int height = input_layer->height();
  int num = input_layer->num();
  float *input_data = input_layer->mutable_cpu_data();
  for (int j = 0; j < num; ++j) {
    std::vector<cv::Mat> input_channels;
    for (int i = 0; i < input_layer->channels(); ++i) {
      cv::Mat channel(height, width, CV_32FC1, input_data);
      input_channels.push_back(channel);
      input_data += width * height;
    }
    input_batch_.push_back(std::vector<cv::Mat>(input_channels));
  }
}

void NeuralNetworkCaffe::preprocess(const cv::Mat& image_, std::vector<cv::Mat>& input_channels_) {
  /* Convert the input image to the input image format of the network. */
  cv::Mat sample;
  if (image_.channels() == 3 && _number_of_channels == 1)
    cv::cvtColor(image_, sample, cv::COLOR_BGR2GRAY);
  else if (image_.channels() == 4 && _number_of_channels == 1)
    cv::cvtColor(image_, sample, cv::COLOR_BGRA2GRAY);
  else if (image_.channels() == 4 && _number_of_channels == 3)
    cv::cvtColor(image_, sample, cv::COLOR_BGRA2BGR);
  else if (image_.channels() == 1 && _number_of_channels == 3)
    cv::cvtColor(image_, sample, cv::COLOR_GRAY2BGR);
  else
    sample = image_;

  cv::Mat sample_resized;
  if (sample.size() != _input_geometry)
    cv::resize(sample, sample_resized, _input_geometry);
  else
    sample_resized = sample;

  cv::Mat sample_float;
  if (_number_of_channels == 3)
    sample_resized.convertTo(sample_float, CV_32FC3);
  else
    sample_resized.convertTo(sample_float, CV_32FC1);

  /* This operation will write the separate BGR planes directly to the
   * input layer of the network because it is wrapped by the cv::Mat
   * objects in input_channels. */
  cv::split(sample_float, input_channels_);
}

void NeuralNetworkCaffe::preprocessBatch(const std::vector<cv::Mat>& images_,
		       std::vector<std::vector<cv::Mat> >& input_batch_) {
  const int number_of_images = images_.size();
  for (int i = 0; i < number_of_images; i++) {
    const cv::Mat &img = images_[i];

    std::vector<cv::Mat> *input_channels = &(input_batch_.at(i));

    /* Convert the input image to the input image format of the network. */
    cv::Mat sample;
    if (img.channels() == 3 && _number_of_channels == 1)
      cv::cvtColor(img, sample, CV_BGR2GRAY);
    else if (img.channels() == 4 && _number_of_channels == 1)
      cv::cvtColor(img, sample, CV_BGRA2GRAY);
    else if (img.channels() == 4 && _number_of_channels == 3)
      cv::cvtColor(img, sample, CV_BGRA2BGR);
    else if (img.channels() == 1 && _number_of_channels == 3)
      cv::cvtColor(img, sample, CV_GRAY2BGR);
    else
      sample = img;

    cv::Mat sample_resized;
    if (sample.size() != _input_geometry)
      cv::resize(sample, sample_resized, _input_geometry);
    else
      sample_resized = sample;

    cv::Mat sample_float;
    if (_number_of_channels == 3)
      sample_resized.convertTo(sample_float, CV_32FC3);
    else
      sample_resized.convertTo(sample_float, CV_32FC1);

    /* This operation will write the separate BGR planes directly to the
     * input layer of the network because it is wrapped by the cv::Mat
     * objects in input_channels. */
    cv::split(sample_float, *input_channels);
  }
}

void NeuralNetworkCaffe::initialize(const std::string &proto_file_,
                                    const std::string &model_file_,
                                    const bool &gpu_mode_) {
  // bdc, set CPU mode, static method
  if (gpu_mode_)
    caffe::Caffe::set_mode(caffe::Caffe::GPU);
  else
    caffe::Caffe::set_mode(caffe::Caffe::CPU);
  // bdc, init the net
  _gl_network.reset(new caffe::Net<float>(proto_file_, caffe::TEST));
  _gl_network->CopyTrainedLayersFrom(model_file_);

  if (gpu_mode_)
    printf("GPU mode enabled\n");
  else
    printf("CPU mode enabled\n");

  _input_geometry = cv::Size(30, 30);
  _number_of_channels = 3;
}

bool NeuralNetworkCaffe::predict(const cv::Mat &image_) {

  std::vector<cv::Mat> input_channels;
  wrapInputLayer(input_channels);
  preprocess(image_, input_channels);
  _gl_network->Forward();

  /* Copy the output layer to a std::vector */
  caffe::Blob<float> *output_layer = _gl_network->output_blobs()[0];
  const float *begin = output_layer->cpu_data();
  const float *end = begin + output_layer->channels();
  std::vector<float> result = std::vector<float>(begin, end);
  if (result.at(0) > result.at(1))
    return false;
  else
    return true;
}

std::vector<bool> binaryClassify(const std::vector<float>& net_output){

  std::vector<bool> net_answer;
  int size = net_output.size();
  int half_size = size/2;

  if(size%2 != 0)
    std::runtime_error("net output does not fit the number of classes!\n");

  net_answer.resize(half_size);
  
  for(int i=0, j=0; i < half_size && j < size; i++, j+=2){
    float false_label = net_output[j];
    float true_label = net_output[j+1];
    if(false_label > true_label)
      net_answer[i] = false;
    else
      net_answer[i] = true;
  }  
  return net_answer;
}
  
std::vector<bool> NeuralNetworkCaffe::predictBatch(const std::vector<cv::Mat> &images_) {

  caffe::Blob<float> *input_layer = _gl_network->input_blobs()[0];

  input_layer->Reshape(images_.size(), _number_of_channels,
                       _input_geometry.height,
                       _input_geometry.width);
  _gl_network->Reshape();
  
  std::vector< std::vector<cv::Mat> > input_batch;

  wrapBatchInputLayer(input_batch);
  preprocessBatch(images_, input_batch);

  _gl_network->Forward();
  
  /* Copy the output layer to a std::vector */
  caffe::Blob<float>* output_layer = _gl_network->output_blobs()[0];
  const float* begin = output_layer->cpu_data();
  const float* end = begin + output_layer->channels()*images_.size();
  std::vector<float> net_output = std::vector<float>(begin, end);

  return binaryClassify(net_output);
  
}

  
}
