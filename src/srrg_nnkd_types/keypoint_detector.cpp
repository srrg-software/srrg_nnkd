#include "keypoint_detector.h"

namespace nnkd {

KeypointDetector::KeypointDetector() {
  std::cerr << "KeypointDetector::KeypointDetector|constructed" << std::endl;
}

void KeypointDetector::configure(const std::string& proto_file_,
                                 const std::string& model_file_,
                                 const bool& gpu_mode_,
                                 const uint32_t& number_of_rows_image_,
                                 const uint32_t& number_of_cols_image_,
                                 const uint32_t& sliding_window_step_size_) {
  std::cerr << "KeypointDetector::KeypointDetector|configuring" << std::endl;

  //ds allocate network instance for given configuration
  _network = new NeuralNetworkCaffe();
  _network->initialize(proto_file_, model_file_, gpu_mode_);

  //ds set detection properties
  number_of_rows_image = number_of_rows_image_;
  number_of_cols_image = number_of_cols_image_;
  sliding_window_step_size = sliding_window_step_size_;
  std::cerr << "KeypointDetector::KeypointDetector|configured" << std::endl;
}

KeypointDetector::~KeypointDetector() {
  std::cerr << "KeypointDetector::KeypointDetector|destroying" << std::endl;
  delete _network;
  std::cerr << "KeypointDetector::KeypointDetector|destroyed" << std::endl;
}

void KeypointDetector::detect(const cv::Mat& intensity_image_, std::vector<cv::KeyPoint>& keypoints_) {

  //ds empty input vector
  keypoints_.clear();

  //bdc can be done better with row and col pointer TODO
  for (uint32_t row = sliding_window_row_init; row <= number_of_rows_image-sliding_window_row_init; row += sliding_window_step_size) {
    for (uint32_t col = sliding_window_col_init; col <= number_of_cols_image-sliding_window_col_init; col += sliding_window_step_size) {

      //bdc extracting patch
      const cv::Mat& patch = intensity_image_(cv::Rect(col-sliding_window_col_init, row-sliding_window_row_init, sliding_window_cols, sliding_window_rows));

      //ds if the current patch is classified
      if (_network->predict(patch)) {

        //ds register the current patch as keypoint and store it TODO this operation is slow - maybe preallocate and resize
        keypoints_.push_back(cv::KeyPoint(col, row, keypoint_size_pixels));
      }
    }
  }
}

void KeypointDetector::detectBatch(const cv::Mat& intensity_image_, std::vector<cv::KeyPoint>& keypoints_) {

  //bdc this method is bit dirty, we need to store all the patches in a vector
  //    and then test the net.
  std::vector<cv::KeyPoint> all_keypoints;
  std::vector<cv::Mat> grouped_patches;
  
  //ds empty input vector
  keypoints_.clear();

  //bdc can be done better with row and col pointer TODO
  for (uint32_t row = sliding_window_row_init; row <= number_of_rows_image-sliding_window_row_init; row += sliding_window_step_size) {
    for (uint32_t col = sliding_window_col_init; col <= number_of_cols_image-sliding_window_col_init; col += sliding_window_step_size) {

      //bdc extracting patch and add it to the whole set
      const cv::Mat& patch = intensity_image_(cv::Rect(col-sliding_window_col_init, row-sliding_window_row_init, sliding_window_cols, sliding_window_rows));
      grouped_patches.push_back(patch);

      //bdc generate keypoint and add it to the whole set
      cv::KeyPoint new_keypoint(col, row, keypoint_size_pixels);
      all_keypoints.push_back(new_keypoint);
      
    }
  }

  std::vector<bool> is_keypoint = _network->predictBatch(grouped_patches);
  std::vector<bool>::iterator keyp_it = is_keypoint.begin();
  for(int i=0; keyp_it != is_keypoint.end(); ++keyp_it, ++i) {
    if(*keyp_it)
      keypoints_.push_back(all_keypoints[i]);
  }

}
  
}
