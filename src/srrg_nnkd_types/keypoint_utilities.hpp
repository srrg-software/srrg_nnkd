#pragma once
#include <opencv2/core/version.hpp>
#include <opencv2/opencv.hpp>
#if CV_MAJOR_VERSION == 3
#include <opencv/cv.h>
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#endif

//ds cv colors
#define CV_COLOR_BLUE cv::Scalar(255, 0, 0)
#define CV_COLOR_GREEN cv::Scalar(0, 255, 0)
#define CV_COLOR_RED cv::Scalar(0, 0, 255)
#define CV_COLOR_LIGHTBLUE cv::Scalar(255, 128, 0)
#define CV_COLOR_ORANGE cv::Scalar(0, 128, 255)

//ds labelling
#define LABEL_GOOD 1
#define LABEL_BAD 0

// ds target descriptor byte size
#define DESCRIPTOR_SIZE_BYTES 32

void extractGoodPatches(std::vector<cv::KeyPoint>& good_Features,
                        const std::vector<cv::KeyPoint>& keypoints,
                        const int patch_size,
                        const cv::Mat& gray_image,
                        const int target_number_of_keypoints,
                        const std::string& image_filename,
                        const std::string& labelled_image_filename,
                        const std::string& feature_type,
                        std::ofstream& association_writer) {

  int number_of_stored_keypoints = 0;
  int rows = gray_image.rows;
  int cols = gray_image.cols;
  for (const cv::KeyPoint &keypoint : keypoints) {
    if (keypoint.pt.y > patch_size && keypoint.pt.y + patch_size < rows &&
        keypoint.pt.x > patch_size && keypoint.pt.x + patch_size < cols) {
      cv::Mat cropedImage = gray_image(
      cv::Rect(keypoint.pt.x - patch_size / 2, keypoint.pt.y - patch_size / 2, patch_size, patch_size));

      //absolute path
      std::string crop_image_name = labelled_image_filename + "_" + std::to_string(number_of_stored_keypoints) + feature_type + ".png";

      imwrite(crop_image_name, cropedImage);

      //relative path
      std::string labeled_image_filename = image_filename + "_" + std::to_string(number_of_stored_keypoints) + feature_type + ".png";

      association_writer << labeled_image_filename << " " << LABEL_GOOD << std::endl;

      number_of_stored_keypoints++;

      good_Features.push_back(keypoint);
    }
    if (target_number_of_keypoints == number_of_stored_keypoints) {
      return;
    }
  }
}

void extractBadPatches(std::vector<cv::Point_<float> >& bad_Features,
                       const std::vector<std::vector<cv::KeyPoint> >& keypoints_vector,
                       const uint32_t& patch_size,
                       const cv::Mat& gray_image,
                       const uint32_t& target_number_of_keypoints,
                       const std::string& image_filename,
                       const std::string& nok_labelled_image_filename,
                       std::ofstream& association_writer) {

  // ds number of stored non-keypoints for background training
  uint32_t number_of_stored_non_keypoints = 0;

  int rows = gray_image.rows;
  int cols = gray_image.cols;

  // bdc, extracting and saving patches without keypoints
  uint32_t maximum_number_of_iterations = 0;
  while (number_of_stored_non_keypoints < target_number_of_keypoints &&
         maximum_number_of_iterations < 1000) {
    ++maximum_number_of_iterations;
    // random pick of a pixel
    int rand_row = std::rand() % (rows - 2 * patch_size) + patch_size;
    int rand_col = std::rand() % (cols - 2 * patch_size) + patch_size;

    bool keep = true;
    // check that it is not close to a keypoint
    // bdc, this iterator was needed to break the double for using `keep`
    std::vector<std::vector<cv::KeyPoint>>::const_iterator
        keypoint_vector_iterator = keypoints_vector.begin();
    for (; keypoint_vector_iterator != keypoints_vector.end() && keep;
         ++keypoint_vector_iterator) {
      const std::vector<cv::KeyPoint> &keypoints =
          (*keypoint_vector_iterator);
      for (const cv::KeyPoint &keypoint : keypoints) {
        if (sqrt((keypoint.pt.x - rand_row) * (keypoint.pt.x - rand_row) +
                 (keypoint.pt.y - rand_col) * (keypoint.pt.y - rand_col)) <
            2 * patch_size) {
          keep = false;
          break;
        }
      }
    }
    if (keep) {
      bad_Features.push_back(cv::Point_<float>((float)rand_col, (float)rand_row));
      cv::Mat cropedImage =
          gray_image(cv::Rect(rand_col, rand_row, patch_size, patch_size));
      std::string crop_image_name =
          nok_labelled_image_filename + "_" +
          std::to_string(number_of_stored_non_keypoints) + ".png";
      imwrite(crop_image_name, cropedImage);
      std::string labeled_image_filename =
          "NOK_" + image_filename + "_" +
          std::to_string(number_of_stored_non_keypoints) + ".png";
      association_writer << labeled_image_filename << " "
                         << LABEL_BAD << std::endl;
      ++number_of_stored_non_keypoints;
    }
  }
}

void drawKeyPoints(cv::Mat &image,
                   cv::Mat &labelled_color_image,
                   const std::vector<cv::KeyPoint> &keypoints,
                   const cv::Scalar& color,
                   const uint32_t& patch_size = 30) {

  cv::Point_<float> offset(patch_size / 2, patch_size / 2);
  for (const cv::KeyPoint &keypoint : keypoints) {
    cv::rectangle(image, keypoint.pt - offset, keypoint.pt + offset, color);
    cv::circle(image, keypoint.pt, 1, color);
    cv::rectangle(labelled_color_image, keypoint.pt - offset, keypoint.pt + offset, color);
    cv::circle(labelled_color_image, keypoint.pt, 1, color);
  }
}

void drawPoints(cv::Mat &image,
                cv::Mat &labelled_color_image,
                const std::vector<cv::Point_<float> > &points,
                const cv::Scalar& color,
                const uint32_t& patch_size = 30) {

  cv::Point_<float> offset(patch_size / 2, patch_size / 2);
  for (const cv::Point_<float> &point : points) {
    cv::rectangle(image, point - offset, point + offset, color);
    cv::circle(image, point, 1, color);
    cv::rectangle(labelled_color_image, point - offset, point + offset, color);
    cv::circle(labelled_color_image, point, 1, color);
  }
}
