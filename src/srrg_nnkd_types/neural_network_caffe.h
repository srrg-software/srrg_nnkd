#pragma once
#include <caffe/caffe.hpp>
#include <opencv2/core/version.hpp>
#include <opencv2/opencv.hpp>

#if CV_MAJOR_VERSION == 3
#include <opencv/cv.h>
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>
#endif

namespace nnkd {

//! @class wrapped neural network instance
class NeuralNetworkCaffe {

// ds object life
public:

  //! @brief default constructor
  NeuralNetworkCaffe(): _gl_network(0),
                        _input_geometry(cv::Size(0, 0)),
                        _number_of_channels(0) {};

  //! @brief default destructor
  ~NeuralNetworkCaffe() {};

// ds access
public:

  //! @brief wrap input layer
  /* Wrap the input layer of the network in separate cv::Mat objects
   * (one per channel). This way we save one memcpy operation and we
   * don't need to rely on cudaMemcpy2D. The last preprocessing
   * operation will write the separate channels directly to the input
   * layer. */
  //! @param[in,out] input_channels_
  void wrapInputLayer(std::vector<cv::Mat>& input_channels_);

  //! @brief batch version of wrapInputLayer
  /* Wrap the input layer of the network in separate cv::Mat objects
   * (one per channel). This way we save one memcpy operation and we
   * don't need to rely on cudaMemcpy2D. The last preprocessing
   * operation will write the separate channels directly to the input
   * layer. */
  //! @param[in,out] input_batch_
  void wrapBatchInputLayer(std::vector<std::vector<cv::Mat> >& input_batch_);

  
  //! @brief preprocess
  //! @param[in] image_
  //! @param[in,out] input_channels_
  void preprocess(const cv::Mat& image_, std::vector<cv::Mat>& input_channels_);

  //! @brief preprocess batch version
  //! @param[in] images_
  //! @param[in,out] input_channels_
  void preprocessBatch(const std::vector<cv::Mat>& images_,
		       std::vector<std::vector<cv::Mat> >& input_batch_);

  
  //! @brief netInit
  /* initialize the gl_net from the .prototxt
   * and .caffemodel files.
   */
  //! @param[in] proto_file_
  //! @param[in] model_file_
  //! @param[in] gpu_mode_
  void initialize(const std::string& proto_file_,
                  const std::string& model_file_, const bool& gpu_mode_);

  //! @brief predict
  /* preforms prediction(forward) of an image(a patch)
   * return true if the image is a keypoint
   */
  //! @param[in] image_
  //! @returns whether the provided image patch could be classified or not
  bool predict(const cv::Mat& image_);

  std::vector<bool> predictBatch(const std::vector<cv::Mat> &images_);

// ds attributes
private:

  //! @brief
  std::shared_ptr<caffe::Net<float>> _gl_network;

  //! @brief
  cv::Size _input_geometry;

  //! @brief
  uint32_t _number_of_channels;
};
}
