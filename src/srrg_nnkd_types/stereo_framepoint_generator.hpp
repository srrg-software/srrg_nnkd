#pragma once
#include <opencv2/core/version.hpp>
#include <opencv2/opencv.hpp>

#if CV_MAJOR_VERSION == 3
#include <opencv/cv.h>
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>
#endif

#include <stdint.h>

class StereoFramePointGenerator {

//ds exported types
public:

  //ds container holding spatial and appearance information (used in findStereoKeypoints)
  struct KeypointWithDescriptor {
    cv::KeyPoint keypoint;
    cv::Mat descriptor;
    int32_t row; //ds keypoint v coordinate
    int32_t col; //ds keypoint u coordinate
  };

  //ds a 2d array of pointers to KeypointWithDescriptor
  typedef KeypointWithDescriptor*** KeypointWithDescriptorPointerMatrix;

//ds object life cycle
public:

  StereoFramePointGenerator():
#if CV_MAJOR_VERSION == 2
                               _descriptor_extractor(new cv::BriefDescriptorExtractor(32)),
#elif CV_MAJOR_VERSION == 3
                               _descriptor_extractor(cv::xfeatures2d::BriefDescriptorExtractor::create(32)),
#else
#error OpenCV version not supported
#endif
                               _number_of_available_points(0),
                               _maximum_matching_distance(50) {
    _keypoints_with_descriptors_left.clear();
    _keypoints_with_descriptors_right.clear();
  };

  ~StereoFramePointGenerator() {
#if CV_MAJOR_VERSION == 2
    delete _descriptor_extractor;
#endif
  };

//ds access
public:

  //ds extracts the defined descriptors for the given keypoints (called within compute)
  void extractDescriptors(const cv::Mat& intensity_image_, std::vector<cv::KeyPoint>& keypoints_, cv::Mat& descriptors_) {
    _descriptor_extractor->compute(intensity_image_, keypoints_, descriptors_);
  }

  //ds initializes structures for the epipolar stereo keypoint search (called within compute)
  void initialize(const std::vector<cv::KeyPoint>& keypoints_left_,
                  const std::vector<cv::KeyPoint>& keypoints_right_,
                  const cv::Mat& descriptors_left_,
                  const cv::Mat& descriptors_right_) {

    //ds prepare keypoint with descriptors vectors for stereo keypoint search
    _keypoints_with_descriptors_left.resize(keypoints_left_.size());
    _keypoints_with_descriptors_right.resize(keypoints_right_.size());
    if (keypoints_left_.size() <= keypoints_right_.size()) {
      for (uint64_t u = 0; u < keypoints_left_.size(); ++u) {
        _keypoints_with_descriptors_left[u].keypoint    = keypoints_left_[u];
        _keypoints_with_descriptors_left[u].descriptor  = descriptors_left_.row(u);
        _keypoints_with_descriptors_left[u].row         = keypoints_left_[u].pt.y;
        _keypoints_with_descriptors_left[u].col         = keypoints_left_[u].pt.x;
        _keypoints_with_descriptors_right[u].keypoint   = keypoints_right_[u];
        _keypoints_with_descriptors_right[u].descriptor = descriptors_right_.row(u);
        _keypoints_with_descriptors_right[u].row        = keypoints_right_[u].pt.y;
        _keypoints_with_descriptors_right[u].col        = keypoints_right_[u].pt.x;
      }
      for (uint64_t u = keypoints_left_.size(); u < keypoints_right_.size(); ++u) {
        _keypoints_with_descriptors_right[u].keypoint   = keypoints_right_[u];
        _keypoints_with_descriptors_right[u].descriptor = descriptors_right_.row(u);
        _keypoints_with_descriptors_right[u].row        = keypoints_right_[u].pt.y;
        _keypoints_with_descriptors_right[u].col        = keypoints_right_[u].pt.x;
      }
    } else {
      for (uint64_t u = 0; u < keypoints_right_.size(); ++u) {
        _keypoints_with_descriptors_left[u].keypoint    = keypoints_left_[u];
        _keypoints_with_descriptors_left[u].descriptor  = descriptors_left_.row(u);
        _keypoints_with_descriptors_left[u].row         = keypoints_left_[u].pt.y;
        _keypoints_with_descriptors_left[u].col         = keypoints_left_[u].pt.x;
        _keypoints_with_descriptors_right[u].keypoint   = keypoints_right_[u];
        _keypoints_with_descriptors_right[u].descriptor = descriptors_right_.row(u);
        _keypoints_with_descriptors_right[u].row        = keypoints_right_[u].pt.y;
        _keypoints_with_descriptors_right[u].col        = keypoints_right_[u].pt.x;
      }
      for (uint64_t u = keypoints_right_.size(); u < keypoints_left_.size(); ++u) {
        _keypoints_with_descriptors_left[u].keypoint   = keypoints_left_[u];
        _keypoints_with_descriptors_left[u].descriptor = descriptors_left_.row(u);
        _keypoints_with_descriptors_left[u].row        = keypoints_left_[u].pt.y;
        _keypoints_with_descriptors_left[u].col        = keypoints_left_[u].pt.x;
      }
    }
  }

  //ds computes all potential stereo keypoints (exhaustive in matching distance) and stores them as framepoints (called within compute)
  void findStereoKeypoints() {

    //ds sort all input vectors by ascending row positions
    std::sort(_keypoints_with_descriptors_left.begin(), _keypoints_with_descriptors_left.end(), [](const KeypointWithDescriptor& a_, const KeypointWithDescriptor& b_){return ((a_.row < b_.row) || (a_.row == b_.row && a_.col < b_.col));});
    std::sort(_keypoints_with_descriptors_right.begin(), _keypoints_with_descriptors_right.end(), [](const KeypointWithDescriptor& a_, const KeypointWithDescriptor& b_){return ((a_.row < b_.row) || (a_.row == b_.row && a_.col < b_.col));});

    //ds running variable
    uint64_t idx_R = 0;

    //ds loop over all left keypoints
    _number_of_available_points = 0;
    for (uint64_t idx_L = 0; idx_L < _keypoints_with_descriptors_left.size(); idx_L++) {
      //stop condition
      if (idx_R == _keypoints_with_descriptors_right.size()) {break;}
      //the right keypoints are on an lower row - skip left
      while (_keypoints_with_descriptors_left[idx_L].row < _keypoints_with_descriptors_right[idx_R].row) {
        idx_L++; if (idx_L == _keypoints_with_descriptors_left.size()) {break;}
      }
      if (idx_L == _keypoints_with_descriptors_left.size()) {break;}
      //the right keypoints are on an upper row - skip right
      while (_keypoints_with_descriptors_left[idx_L].row > _keypoints_with_descriptors_right[idx_R].row) {
        idx_R++; if (idx_R == _keypoints_with_descriptors_right.size()) {break;}
      }
      if (idx_R == _keypoints_with_descriptors_right.size()) {break;}
      //search bookkeeping
      uint64_t idx_RS = idx_R;
      double dist_best = _maximum_matching_distance;
//      uint64_t idx_best_R = 0;
      //scan epipolar line for current keypoint at idx_L
      while (_keypoints_with_descriptors_left[idx_L].row == _keypoints_with_descriptors_right[idx_RS].row) {
        //zero disparity stop condition
        if (_keypoints_with_descriptors_right[idx_RS].col >= _keypoints_with_descriptors_left[idx_L].col) {break;}
        //compute descriptor distance
        const double dist = cv::norm(_keypoints_with_descriptors_left[idx_L].descriptor, _keypoints_with_descriptors_right[idx_RS].descriptor, cv::NORM_HAMMING);
        if(dist < dist_best) {
          dist_best = dist;
//          idx_best_R = idx_RS;
        }
        idx_RS++; if (idx_RS == _keypoints_with_descriptors_right.size()) {break;}
      }
      //check if something was found
      if (dist_best < _maximum_matching_distance) {
        ++_number_of_available_points;
      }
    }
  }

//ds getters/setters
public:

  const uint64_t& numberOfAvailablePoints() const {return _number_of_available_points;}
  const double& maximumMatchingDistance() const {return _maximum_matching_distance;}

//ds attributes:
private:

  //ds descriptor extraction
#if CV_MAJOR_VERSION == 2
  const cv::DescriptorExtractor* _descriptor_extractor;
#elif CV_MAJOR_VERSION == 3
  cv::Ptr<cv::DescriptorExtractor> _descriptor_extractor;
#else
#error OpenCV version not supported
#endif

  //ds point detection properties
  uint64_t _number_of_available_points;

  double _maximum_matching_distance;

  //! @brief inner memory buffers (operated on in compute, kept here for readability)
  std::vector<KeypointWithDescriptor> _keypoints_with_descriptors_left;
  std::vector<KeypointWithDescriptor> _keypoints_with_descriptors_right;
};
