#include "srrg_messages/message_reader.h"
#include "srrg_messages/pinhole_image_message.h"
#include "srrg_nnkd_types/keypoint_detector.h"

//ds test program for caffe based framepoint generator
int32_t main(int32_t argc, char** argv) {

  //ds initialize google logging TODO blast this
  google::InitGoogleLogging(argv[0]);

  //ds check minimum parameter number
  if (argc < 2) {
    std::cerr << "git gud!" << std::endl;
    return 0;
  }

  //ds command line parameters
  std::string topic_images_left     = "/camera_left/image_raw";
  std::string proto_path            = "glnet_deploy.prototxt";
  std::string model_path            = "100.caffemodel";
  std::string dataset_path          = "";
  bool gpu_mode                     = false;
  uint32_t sliding_window_step_size = 10;

  //ds parse parameters
  int32_t parameter_count = 1;
  while (parameter_count < argc) {
    if (!strcmp(argv[parameter_count], "-h")) {
      std::cerr << "git gud!" << std::endl;
      return 1;
    } else if (!strcmp(argv[parameter_count], "-l")) {
      parameter_count++;
      topic_images_left = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-proto")) {
      parameter_count++;
      proto_path = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-model")) {
      parameter_count++;
      model_path = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-gpu")) {
      gpu_mode = true;
    } else if (!strcmp(argv[parameter_count], "-sw")) {
      parameter_count++;
      sliding_window_step_size = std::stoi(argv[parameter_count]);
    } else {
      dataset_path = argv[parameter_count];
    }
    parameter_count++;
  }

  //ds validate parameters
  if (proto_path.empty()) {
    std::cerr << "please provide a valid prototxt file" << std::endl;
    return 0;
  }
  if (model_path.empty()) {
    std::cerr << "please provide a valid caffemodel file" << std::endl;
    return 0;
  }
  if (dataset_path.empty()) {
    std::cerr << "please provide a valid dataset file" << std::endl;
    return 0;
  }

  //ds info
  std::cerr << "images left:         " << topic_images_left << std::endl;
  std::cerr << "caffe net prototype: " << proto_path << std::endl;
  std::cerr << "trained model:       " << model_path << std::endl;
  std::cerr << "dataset:             " << dataset_path << std::endl;
  std::cerr << "mode:                "; (gpu_mode) ? std::cerr << "GPU" : std::cerr << "CPU";
  std::cerr << std::endl;

  //ds get a detector instance
  nnkd::KeypointDetector* detector = new nnkd::KeypointDetector();

  //ds configure the detector
  detector->configure(proto_path, model_path, false, 0, 0, sliding_window_step_size);

  //bdc txtio reader
  srrg_core::MessageReader message_reader;
  message_reader.open(dataset_path);
  srrg_core::BaseMessage *base_message = 0;

  //bdc playback message file
  while ((base_message = message_reader.readMessage())) {

    //ds load message
    srrg_core::BaseSensorMessage* sensor_message = dynamic_cast<srrg_core::BaseSensorMessage*>(base_message);
    if (!sensor_message) {
      delete base_message;
      continue;
    } else {
      sensor_message->untaint();
    }

    //ds check if fitting
    if (sensor_message->topic() != topic_images_left) {
      delete sensor_message;
      continue;
    }

    //ds buffer sensor data
    srrg_core::PinholeImageMessage* message_image_left  = dynamic_cast<srrg_core::PinholeImageMessage*>(sensor_message);

    //ds retrieve image stats
    const cv::Mat& image_left = message_image_left->image();
    detector->setNumberOfRowsImage(image_left.rows);
    detector->setNumberOfColsImage(image_left.cols);

    //ds keypoint buffer
    std::vector<cv::KeyPoint> keypoints(0);

    //ds detect points
    detector->detectBatch(image_left, keypoints);

    //ds get a colorized copy of the current image
    cv::Mat display_image_rgb;
    cv::cvtColor(image_left, display_image_rgb, CV_GRAY2RGB);

    //ds show image and extracted keypoints
    for (const cv::KeyPoint& keypoint: keypoints) {
      cv::circle(display_image_rgb, keypoint.pt, 2, cv::Scalar(0, 0, 255), -1);
    }
    cv::imshow("tracking", display_image_rgb);
    cv::waitKey(1);

    //ds info
    std::cerr << "detected points: " << keypoints.size() << std::endl;
  }
  message_reader.close();

  //ds done
  delete detector;
  return 0;
}
