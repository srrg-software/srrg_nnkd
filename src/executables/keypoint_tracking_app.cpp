#include "srrg_system_utils/system_utils.h"
#include "srrg_messages/message_reader.h"
#include "srrg_messages/pinhole_image_message.h"
#include "srrg_nnkd_types/neural_network_caffe.h"
#include "srrg_nnkd_types/stereo_framepoint_generator.hpp"

using namespace srrg_core;

//ds visualization only
#define CV_COLOR_BLUE cv::Scalar(255, 0, 0)
#define CV_COLOR_GREEN cv::Scalar(0, 255, 0)
#define CV_COLOR_RED cv::Scalar(0, 0, 255)

const char *banner[] = {
    "\n\nUsage: keypoint_tracking_gl_app -proto <prototxt_model> -model <trained_model> -l <topic_images_left> [Options] <dump_file.txt>\n",
    "Example:  keypoint_tracking_gl_app -proto gl_net.prototxt -model gl_net_iter_10000.caffemodel -l /camera_left/image_raw 00.txt\n\n",
    "Options:\n", "------------------------------------------\n",
    "-proto <string>             network_model.prototxt",
    "-model <string>             network.caffemodel",
    "-l <string>                 intensity image topic name for the left camera, as in the txtio file",
    "-gpu <flag>                 to use GPU mode, default false",
    "-verbose <flag>             google log info, default false",
    "-h                          this help\n", 0};

int32_t main(int32_t argc, char **argv) {

  //ds initialize google logging TODO blast this
  google::InitGoogleLogging(argv[0]);

  //ds check minimum parameter number
  if (argc < 2) {
    printBanner(banner);
    return 0;
  }
 
  //ds command line parameters
  std::string topic_images_left  = "/camera_left/image_raw";
  std::string proto_path         = "";
  std::string model_path         = "";
  std::string dataset_path       = "";
  bool gpu_mode                  = false;
  bool verbose                   = false;
  
  //ds parse parameters
  int32_t parameter_count = 1;
  while (parameter_count < argc) {
    if (!strcmp(argv[parameter_count], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[parameter_count], "-l")) {
      parameter_count++;
      topic_images_left = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-proto")) {
      parameter_count++;
      proto_path = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-model")) {
      parameter_count++;
      model_path = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-gpu")) {
      gpu_mode = true;
    } else if (!strcmp(argv[parameter_count], "-verbose")) {
      verbose = true;
    } else {
      dataset_path = argv[parameter_count];
    }
    parameter_count++;
  }

  //ds validate parameters
  if (proto_path.empty()) {
    std::cerr << "please provide a valid prototxt file" << std::endl;
    return 0;
  }
  if (model_path.empty()) {
    std::cerr << "please provide a valid caffemodel file" << std::endl;
    return 0;
  }
  if (dataset_path.empty()) {
    std::cerr << "please provide a valid dataset file" << std::endl;
    return 0;
  }

  //ds info
  std::cerr << "images left:         " << topic_images_left << std::endl;
  std::cerr << "caffe net prototype: " << proto_path << std::endl;
  std::cerr << "trained model:       " << model_path << std::endl;
  std::cerr << "dataset:             " << dataset_path << std::endl;
  std::cerr << "mode:                "; (gpu_mode) ? std::cerr << "GPU" : std::cerr << "CPU";
  std::cerr << std::endl;

  // bdc, manage google logging verbosity
  if (!verbose) {
    google::SetCommandLineOption("GLOG_minloglevel", "2");
  }
  
  //ds request a neural network instance with default parameters
  nnkd::NeuralNetworkCaffe network;

  // bdc, init the net
  network.initialize(proto_path, model_path, gpu_mode);

  // bdc, txtio reader
  MessageReader message_reader;
  message_reader.open(dataset_path);

  // bdc, read messages
  BaseMessage *base_message = 0;

  //ds tracking - previous keypoints and descriptors
  StereoFramePointGenerator::KeypointWithDescriptorPointerMatrix keypoint_matrix = 0;
  const int32_t maximum_distance_tracking_pixels = 50;

  //ds get a stereo framepoint triangulator
  StereoFramePointGenerator framepoint_generator;
  const float keypoint_size_pixels = 7;

  //ds sliding window configuration on the image
  const uint32_t sliding_window_rows      = 30;
  const uint32_t sliding_window_cols      = 30;
  const uint32_t sliding_window_row_init  = 15;
  const uint32_t sliding_window_col_init  = 15;
  const uint32_t sliding_window_step_size = 5;
  uint32_t image_rows = 0;
  uint32_t image_cols = 0;

  //ds stats
  uint64_t number_of_detected_keypoints_left = 0;
  uint64_t number_of_tracks                  = 0;
  double start_time_seconds                  = srrg_core::getTime();
  uint64_t number_of_frames_current_window   = 0;
  uint64_t number_of_frames_total            = 0;
  const double measurement_interval_seconds  = 2;

  // bdc, reading from TXTIO
  while ((base_message = message_reader.readMessage())) {

    //ds load message
    BaseSensorMessage* sensor_message = dynamic_cast<BaseSensorMessage*>(base_message);
    if (!sensor_message) {
      delete base_message;
      continue;
    } else {
      sensor_message->untaint();
    }
    
    //ds buffer sensor data
    srrg_core::PinholeImageMessage* message_image_left  = dynamic_cast<srrg_core::PinholeImageMessage*>(sensor_message);
    if (!message_image_left || strcmp(message_image_left->topic().c_str(), topic_images_left.c_str())) {
      delete message_image_left;
      continue;
    }
    message_image_left->untaint();

    //bdc get the image in grayscale
    const cv::Mat& image_left = message_image_left->image();
    image_rows                = image_left.rows;
    image_cols                = image_left.cols;

    //ds ugly - check if tracking matrix is not not allocated
    if (!keypoint_matrix) {

      //ds allocate tracking grid
      keypoint_matrix = new StereoFramePointGenerator::KeypointWithDescriptor**[image_left.rows];
      for (uint32_t row = 0; row < image_rows; ++row) {
        keypoint_matrix[row] = new StereoFramePointGenerator::KeypointWithDescriptor*[image_left.cols];
        for (uint32_t col = 0; col < image_cols; ++col) {
          keypoint_matrix[row][col] = 0;
        }
      }
    }

    //ds empty vector
    std::vector<cv::KeyPoint> keypoints_left(0);

    // bdc, can be done better with row and col pointer. <--TODO
    for (uint32_t row = sliding_window_row_init; row <= image_rows-sliding_window_row_init; row += sliding_window_step_size) {
      for (uint32_t col = sliding_window_col_init; col <= image_cols-sliding_window_col_init; col += sliding_window_step_size) {

        // bdc, extracting patch
        const cv::Mat& patch_left = image_left(cv::Rect(col-sliding_window_col_init, row-sliding_window_row_init, sliding_window_cols, sliding_window_rows));

        //ds if the current patch is classified
        if (network.predict(patch_left)) {

          //ds register the current patch as keypoint and store it
          keypoints_left.push_back(cv::KeyPoint(col, row, keypoint_size_pixels));
        }
      }
    }

    //ds extract descriptors
    cv::Mat descriptors_left;
    framepoint_generator.extractDescriptors(image_left, keypoints_left, descriptors_left);

    //ds compute tracks
    std::vector<std::pair<cv::Point2f, cv::Point2f>> tracks(keypoints_left.size());
    uint32_t index_track = 0;

    //ds for each current point
    for (uint32_t index = 0; index < keypoints_left.size(); ++index) {

      //ds buffer position and appearance
      const int32_t& row_current = keypoints_left[index].pt.y;
      const int32_t& col_current = keypoints_left[index].pt.x;
      const cv::Mat& descriptor  = descriptors_left.row(index);

      //ds exhaustive search
      int32_t pixel_distance_best = maximum_distance_tracking_pixels;
      int32_t row_best = -1;
      int32_t col_best = -1;

      //ds compute borders
      const int32_t row_start_region = std::max(row_current-maximum_distance_tracking_pixels, static_cast<int32_t>(0));
      const int32_t row_end_region   = std::min(row_current+maximum_distance_tracking_pixels, static_cast<int32_t>(image_rows));
      const int32_t col_start_region = std::max(col_current-maximum_distance_tracking_pixels, static_cast<int32_t>(0));
      const int32_t col_end_region   = std::min(col_current+maximum_distance_tracking_pixels, static_cast<int32_t>(image_cols));

      //ds locate best match
      for (int32_t row_region = row_start_region; row_region < row_end_region; ++row_region) {
        for (int32_t col_region = col_start_region; col_region < col_end_region; ++col_region) {
          if (keypoint_matrix[row_region][col_region]) {
            const int32_t pixel_distance  = std::fabs(row_current-row_region)+std::fabs(col_current-col_region);
            const float matching_distance = cv::norm(descriptor, keypoint_matrix[row_region][col_region]->descriptor, cv::NORM_HAMMING);
            if (pixel_distance < pixel_distance_best && matching_distance < framepoint_generator.maximumMatchingDistance()) {
              pixel_distance_best = pixel_distance;
              row_best = row_region;
              col_best = col_region;
            }
          }
        }
      }

      //ds if we found a match
      if (pixel_distance_best < maximum_distance_tracking_pixels) {

        //ds add it
        tracks[index_track].first  = keypoint_matrix[row_best][col_best]->keypoint.pt;
        tracks[index_track].second = keypoints_left[index].pt;
        ++index_track;

        //ds disable further matching
        delete keypoint_matrix[row_best][col_best];
        keypoint_matrix[row_best][col_best] = 0;
      }
    }
    tracks.resize(index_track);

    //ds clear old keypoint matrix
    for (uint32_t row = 0; row < image_rows; ++row) {
      for (uint32_t col = 0; col < image_cols; ++col) {
        if (keypoint_matrix[row][col]) {
          delete keypoint_matrix[row][col];
          keypoint_matrix[row][col] = 0;
        }
      }
    }

    //ds add current points
    for (uint32_t index = 0; index < keypoints_left.size(); ++index) {
      const int32_t& row = keypoints_left[index].pt.y;
      const int32_t& col = keypoints_left[index].pt.x;
      keypoint_matrix[row][col] = new StereoFramePointGenerator::KeypointWithDescriptor();
      keypoint_matrix[row][col]->col        = col;
      keypoint_matrix[row][col]->row        = row;
      keypoint_matrix[row][col]->keypoint   = keypoints_left[index];
      keypoint_matrix[row][col]->descriptor = descriptors_left.row(index);
    }

    //ds stats
    number_of_detected_keypoints_left += keypoints_left.size();
    number_of_tracks                  += index_track;

    //ds get a colorized copy of the current image
    cv::Mat display_image_rgb;
    cv::cvtColor(image_left, display_image_rgb, CV_GRAY2RGB);

    // bdc, show image and extracted keypoints
    for (const std::pair<cv::Point2f, cv::Point2f>& track: tracks) {
      cv::line(display_image_rgb, track.first, track.second, CV_COLOR_GREEN);
      cv::circle(display_image_rgb, track.first, 2, CV_COLOR_RED, -1);
      cv::circle(display_image_rgb, track.second, 2, CV_COLOR_BLUE, -1);
    }
    cv::imshow("tracking", display_image_rgb);
    cv::waitKey(1);
    ++number_of_frames_current_window;

    //bdc show stats
    const double duration_seconds = srrg_core::getTime()-start_time_seconds;
    if(duration_seconds > measurement_interval_seconds) {
      number_of_frames_total += number_of_frames_current_window;
      printf("%06lu | fps: %5.2f | keypoints L: %3lu | tracks: %3lu (%4.2f)\n",
             number_of_frames_total, number_of_frames_current_window/duration_seconds,
             number_of_detected_keypoints_left/number_of_frames_current_window,
             number_of_tracks/number_of_frames_current_window,
             static_cast<double>(number_of_tracks)/(number_of_detected_keypoints_left));

      //ds reset stats
      number_of_frames_current_window   = 0;
      number_of_detected_keypoints_left = 0;
      number_of_tracks                  = 0;
      start_time_seconds                = srrg_core::getTime();
    }
  }
  message_reader.close();

  if (keypoint_matrix) {
    for (uint32_t row = 0; row < image_rows; ++row) {
      delete keypoint_matrix[row];
    }
    delete keypoint_matrix;
  }
  return 0;
}
