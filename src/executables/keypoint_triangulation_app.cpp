#include "srrg_system_utils/system_utils.h"
#include "srrg_messages/message_reader.h"
#include "srrg_messages/pinhole_image_message.h"
#include "srrg_messages/message_timestamp_synchronizer.h"
#include "srrg_nnkd_types/neural_network_caffe.h"
#include "srrg_nnkd_types/stereo_framepoint_generator.hpp"

using namespace srrg_core;

//ds visualization only
#define CV_COLOR_BLUE cv::Scalar(255, 0, 0)
#define CV_COLOR_GREEN cv::Scalar(0, 255, 0)
#define CV_COLOR_RED cv::Scalar(0, 0, 255)

const char *banner[] = {
    "\n\nUsage: keypoint_triangulation_gl_app -proto <prototxt_model> -model <trained_model> -l <topic_images_left> -r <topic_images_right> [Options] <dump_file.txt>\n",
    "Example:  keypoint_triangulation_gl_app -proto gl_net.prototxt -model gl_net_iter_10000.caffemodel -l /camera_left/image_raw -r /camera_right/image_raw 00.txt\n\n",
    "Options:\n", "------------------------------------------\n",
    "-proto <string>             network_model.prototxt",
    "-model <string>             network.caffemodel",
    "-l <string>                 intensity image topic name for the left camera, as in the txtio file",
    "-r <string>                 intensity image topic name for the right camera, as in the txtio file",
    "-gpu <flag>                 to use GPU mode, default false",
    "-sw <uint32_t>              sliding window size in pixels",
    "-h                          this help\n", 0};

int32_t main(int32_t argc, char **argv) {

  //ds initialize google logging TODO blast this
  google::InitGoogleLogging(argv[0]);

  //ds check minimum parameter number
  if (argc < 2) {
    printBanner(banner);
    return 0;
  }
 
  //ds command line parameters
  std::string topic_images_left     = "/camera_left/image_raw";
  std::string topic_images_right    = "/camera_right/image_raw";
  std::string proto_path            = "glnet_deploy.prototxt";
  std::string model_path            = "";
  std::string dataset_path          = "";
  bool gpu_mode                     = false;
  uint32_t sliding_window_step_size = 10;
  
  //ds parse parameters
  int32_t parameter_count = 1;
  while (parameter_count < argc) {
    if (!strcmp(argv[parameter_count], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[parameter_count], "-l")) {
      parameter_count++;
      topic_images_left = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-r")) {
      parameter_count++;
      topic_images_right = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-proto")) {
      parameter_count++;
      proto_path = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-model")) {
      parameter_count++;
      model_path = argv[parameter_count];
    } else if (!strcmp(argv[parameter_count], "-gpu")) {
      gpu_mode = true;
    } else if (!strcmp(argv[parameter_count], "-sw")) {
      parameter_count++;
      sliding_window_step_size = std::stoi(argv[parameter_count]);
    } else {
      dataset_path = argv[parameter_count];
    }
    parameter_count++;
  }

  //ds validate parameters
  if (proto_path.empty()) {
    std::cerr << "please provide a valid prototxt file" << std::endl;
    return 0;
  }
  if (model_path.empty()) {
    std::cerr << "please provide a valid caffemodel file" << std::endl;
    return 0;
  }
  if (dataset_path.empty()) {
    std::cerr << "please provide a valid dataset file" << std::endl;
    return 0;
  }

  //ds info
  std::cerr << "images left:         " << topic_images_left << std::endl;
  std::cerr << "images right:        " << topic_images_right << std::endl;
  std::cerr << "caffe net prototype: " << proto_path << std::endl;
  std::cerr << "trained model:       " << model_path << std::endl;
  std::cerr << "dataset:             " << dataset_path << std::endl;
  std::cerr << "mode:                "; (gpu_mode) ? std::cerr << "GPU" : std::cerr << "CPU";
  std::cerr << std::endl;
  
  //ds request a neural network instance with default parameters
  nnkd::NeuralNetworkCaffe network;

  // bdc, init the net
  network.initialize(proto_path, model_path, gpu_mode);

  // bdc, txtio reader
  MessageReader message_reader;
  message_reader.open(dataset_path);

  //ds txt_io message synchronizer
  MessageTimestampSynchronizer synchronizer;
  std::vector<std::string> camera_topics_synchronized(0);
  camera_topics_synchronized.push_back(topic_images_left);
  camera_topics_synchronized.push_back(topic_images_right);
  synchronizer.setTimeInterval(0.001);
  synchronizer.setTopics(camera_topics_synchronized);

  // bdc, read messages
  BaseMessage *base_message = 0;

  //ds buffer vector for extracted keypoints/descriptors
  std::vector<cv::KeyPoint> keypoints_left;
  std::vector<cv::KeyPoint> keypoints_right;
  cv::Mat descriptors_left;
  cv::Mat descriptors_right;

  //ds get a stereo framepoint triangulator
  StereoFramePointGenerator framepoint_generator;
  const float keypoint_size_pixels = 7;

  //ds sliding window configuration on the image
  const uint32_t sliding_window_rows      = 30;
  const uint32_t sliding_window_cols      = 30;
  const uint32_t sliding_window_row_init  = 15;
  const uint32_t sliding_window_col_init  = 15;

  //ds stats
  uint64_t number_of_detected_keypoints_left  = 0;
  uint64_t number_of_detected_keypoints_right = 0;
  uint64_t number_of_stereo_matches           = 0;
  double start_time_seconds                   = srrg_core::getTime();
  uint64_t number_of_frames_current_window    = 0;
  uint64_t number_of_frames_total             = 0;
  const double measurement_interval_seconds   = 2;

  //bdc reading from TXTIO
  while ((base_message = message_reader.readMessage())) {

    //ds load message
    BaseSensorMessage* sensor_message = dynamic_cast<BaseSensorMessage*>(base_message);
    if (!sensor_message) {
      delete base_message;
      continue;
    } else {
      sensor_message->untaint();
    }

    //ds add to synchronizer
    if (sensor_message->topic() == topic_images_left) {
      synchronizer.putMessage(sensor_message);
    } else if (sensor_message->topic() == topic_images_right) {
      synchronizer.putMessage(sensor_message);
    } else {
      delete sensor_message;
    }
    
    //ds if we have a synchronized pair of sensor messages ready
    if (synchronizer.messagesReady()) {

      //ds buffer sensor data
      srrg_core::PinholeImageMessage* message_image_left  = dynamic_cast<srrg_core::PinholeImageMessage*>(synchronizer.messages()[0].get());
      srrg_core::PinholeImageMessage* message_image_right = dynamic_cast<srrg_core::PinholeImageMessage*>(synchronizer.messages()[1].get());

      // bdc, get the image in grayscale
      const cv::Mat& image_left  = message_image_left->image();
      const cv::Mat& image_right = message_image_right->image();
      const uint32_t& image_rows = image_left.rows;
      const uint32_t& image_cols = image_left.cols;

      //ds empty buffered vector
      keypoints_left.clear();
      keypoints_right.clear();

      // bdc, can be done better with row and col pointer. <--TODO
      for (uint32_t row = sliding_window_row_init; row <= image_rows-sliding_window_row_init; row += sliding_window_step_size) {
        for (uint32_t col = sliding_window_col_init; col <= image_cols-sliding_window_col_init; col += sliding_window_step_size) {

          // bdc, extracting patch
          const cv::Mat& patch_left = image_left(cv::Rect(col-sliding_window_col_init, row-sliding_window_row_init, sliding_window_cols, sliding_window_rows));

          //ds if the current patch is classified
          if (network.predict(patch_left)) {

            //ds register the current patch as keypoint and store it
            keypoints_left.push_back(cv::KeyPoint(col, row, keypoint_size_pixels));
          }

          //ds check right
          const cv::Mat& patch_right = image_right(cv::Rect(col-sliding_window_col_init, row-sliding_window_row_init, sliding_window_cols, sliding_window_rows));

          //ds if the current patch is classified
          if (network.predict(patch_right)) {

            //ds register the current patch as keypoint and store it
            keypoints_right.push_back(cv::KeyPoint(col, row, keypoint_size_pixels));
          }
        }
      }

      //ds compute descriptors for the keypoints (potentially shrinking the number of keypoints)
      framepoint_generator.extractDescriptors(image_left, keypoints_left, descriptors_left);
      framepoint_generator.extractDescriptors(image_right, keypoints_right, descriptors_right);

      //ds initialize matcher
      framepoint_generator.initialize(keypoints_left, keypoints_right, descriptors_left, descriptors_right);

      //ds retrieve stereo matches
      framepoint_generator.findStereoKeypoints();

      //ds stats
      number_of_detected_keypoints_left  += keypoints_left.size();
      number_of_detected_keypoints_right += keypoints_right.size();
      number_of_stereo_matches           += framepoint_generator.numberOfAvailablePoints();

      //ds get a colorized copy of the current image
      cv::Mat display_image_rgb;
      cv::cvtColor(image_left, display_image_rgb, CV_GRAY2RGB);

      // bdc, show image and extracted keypoints
      const cv::Point2f offset(sliding_window_rows/2.0, sliding_window_cols/2.0);
      for (const cv::KeyPoint &keypoint: keypoints_left) {
        cv::circle(display_image_rgb, keypoint.pt, 2, CV_COLOR_GREEN, -1);
        cv::rectangle(display_image_rgb, keypoint.pt - offset, keypoint.pt + offset, CV_COLOR_BLUE);
      }
      cv::imshow("triangulation", display_image_rgb);
      cv::waitKey(1);
      ++number_of_frames_current_window;

      //bdc show stats
      const double duration_seconds = srrg_core::getTime()-start_time_seconds;
      if(duration_seconds > measurement_interval_seconds) {
        number_of_frames_total += number_of_frames_current_window;
        printf("%06lu | fps: %5.2f | keypoints L/R: %3lu/%3lu | stereo matches: %3lu (%4.2f)\n",
               number_of_frames_total, number_of_frames_current_window/duration_seconds,
               number_of_detected_keypoints_left/number_of_frames_current_window,
               number_of_detected_keypoints_right/number_of_frames_current_window,
               number_of_stereo_matches/number_of_frames_current_window,
               2.0*number_of_stereo_matches/(number_of_detected_keypoints_left+number_of_detected_keypoints_right));

        //ds reset stats
        number_of_frames_current_window    = 0;
        number_of_detected_keypoints_left  = 0;
        number_of_detected_keypoints_right = 0;
        number_of_stereo_matches           = 0;
        start_time_seconds                 = srrg_core::getTime();
      }
    }
  }
  message_reader.close();
  return 0;
}
