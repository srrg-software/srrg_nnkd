#include "srrg_messages/message_reader.h"
#include "srrg_messages/pinhole_image_message.h"
#include "srrg_system_utils/system_utils.h"

#include <fstream>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "srrg_nnkd_types/keypoint_utilities.hpp"

using namespace srrg_core;

const char *banner[] = {
    "\n\nUsage:  keypoint_generation_app -t <rgb_topic> -ass "
    "<association_file> -out <labeled_folder> [Options] <dump_file.txt>\n",
    "Example:  generate_keypoints_app -t /camera/rgb/image_raw -ass train.txt "
    "-out trainannot dump_file.txt\n\n",
    "Options:\n", "------------------------------------------\n",
    "-t <string>                 RGB image topic name, as in the txtio file",
    "-ass <string>               file where to write the association between "
    "images",
    "-out <string>               output directory. It has to exist!",
    "-orb                        flag to use ORB keypoints",
    "-fast                       flag to use FAST keypoints",
    "-sift                       flag to use SIFT keypoints",
    "-nk                         target number of keypoints to export, default 100",
    "-patch                      patch size of the extracted square images in pixel, default 30",
    "-h                          this help\n", 0};

int32_t main(int32_t argc, char **argv) {

  if (argc < 2) {
    printBanner(banner);
    return 1;
  }

  std::string topic_name_image = "/camera_left/image_raw";
  std::string file_associations = "associations.txt";
  std::string folder_labels = "labels";
  std::string file_messages = "";
  uint32_t patch_size                    = 30; //pixels
  uint32_t number_of_different_keypoints = 0;

  // ds target maximum number of keypoints used for learning per image
  uint32_t target_number_of_keypoints = 100;
  // bdc set true to use the corresponding features
  bool orb = false;
  bool fast = false;
  bool sift = false;

  // ds parse command line
  int32_t number_of_parsed_arguments = 1;
  while (number_of_parsed_arguments < argc) {
    if (!strcmp(argv[number_of_parsed_arguments], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[number_of_parsed_arguments], "-t")) {
      number_of_parsed_arguments++;
      topic_name_image = argv[number_of_parsed_arguments];
    } else if (!strcmp(argv[number_of_parsed_arguments], "-orb")) {
      orb = true;
      number_of_different_keypoints++;
    } else if (!strcmp(argv[number_of_parsed_arguments], "-fast")) {
      fast = true;
      number_of_different_keypoints++;
    } else if (!strcmp(argv[number_of_parsed_arguments], "-sift")) {
      sift = true;
      number_of_different_keypoints++;
    } else if (!strcmp(argv[number_of_parsed_arguments], "-ass")) {
      number_of_parsed_arguments++;
      file_associations = argv[number_of_parsed_arguments];
    } else if (!strcmp(argv[number_of_parsed_arguments], "-out")) {
      number_of_parsed_arguments++;
      folder_labels = argv[number_of_parsed_arguments];
    } else if (!strcmp(argv[number_of_parsed_arguments], "-nk")) {
      number_of_parsed_arguments++;
      target_number_of_keypoints = std::stoi(argv[number_of_parsed_arguments]);
    } else if (!strcmp(argv[number_of_parsed_arguments], "-patch")) {
      number_of_parsed_arguments++;
      patch_size = std::stoi(argv[number_of_parsed_arguments]);
    } else {
      file_messages = argv[number_of_parsed_arguments];
    }
    number_of_parsed_arguments++;
  }

  // bdc, check on chosen features
  if(!fast && !orb && !sift) {
    std::cerr << "WARNING: No feature selected. Using only Fast by default" << std::endl;
    fast = true;
    number_of_different_keypoints++;
  }
  
  // ds escape if no message file was specified
  if (file_messages.length() == 0) {
    std::printf("ERROR: no message file specified\n");
  }

  // bdc, check if output folder (for the labelled images) exists
  struct stat info;

  if (stat(folder_labels.c_str(), &info) != 0) {
    printf("ERROR: cannot access '%s'\n", folder_labels.c_str());
    return 0;
  } else if (info.st_mode & S_IFDIR) {
    //    printf("'%s' is a directory\n", labeled_img_folder.c_str());
  } else {
    printf("ERROR: '%s' is not a directory\n", folder_labels.c_str());
    return 0;
  }

  // bdc, open association output file
  std::ofstream association_writer(file_associations);

  // ds check failure
  if (!association_writer.is_open() || !association_writer.good()) {
    std::printf("ERROR: unable to open association file: '%s'\n",
                file_associations.c_str());
  }

  // ds log configuration
  std::cerr << "topic name:   " << topic_name_image << std::endl;
  std::cerr << "associations: " << file_associations << std::endl;
  std::cerr << "labels:       " << folder_labels << std::endl;
  std::cerr << "messages:     " << file_messages << std::endl;
  std::cerr << "keypoints:    " << target_number_of_keypoints << std::endl;

  // bdc, random seed (time based) to extract patches on image containing no
  // keypoint
  srand(time(NULL));

// ds keypoint detection
#if CV_MAJOR_VERSION == 2
  cv::FeatureDetector *detector_FAST;
  if(fast)
    detector_FAST = new cv::FastFeatureDetector(50);

  cv::FeatureDetector *detector_ORB;
  if(orb)
    detector_ORB = new cv::ORB(10000);

  cv::FeatureDetector *detector_SIFT;
  if(sift)
    detector_SIFT = new cv::SIFT(10000);

#elif CV_MAJOR_VERSION == 3
  cv::Ptr<cv::FeatureDetector> detector_FAST(
      cv::FastFeatureDetector::create(50));
  cv::Ptr<cv::FeatureDetector> detector_ORB(cv::ORB::create(10000));
  cv::Ptr<cv::FeatureDetector> detector_SIFT(cv::xfeatures2d::SIFT::create(10000));
#endif

// ds descriptor extraction
#if CV_MAJOR_VERSION == 2
  const cv::DescriptorExtractor *descriptor_extractor =
      new cv::BriefDescriptorExtractor(DESCRIPTOR_SIZE_BYTES);
#elif CV_MAJOR_VERSION == 3
  cv::Ptr<cv::DescriptorExtractor> descriptor_extractor =
      cv::xfeatures2d::BriefDescriptorExtractor::create(DESCRIPTOR_SIZE_BYTES);
#endif

 
  // bdc, txtio reader
  MessageReader reader;
  reader.open(file_messages);

  // bdc, read messages
  BaseMessage *msg = 0;

  // ds info
  uint64_t number_of_processed_messages = 0;
  while ((msg = reader.readMessage())) {
    BaseSensorMessage *sensor_msg = dynamic_cast<BaseSensorMessage *>(msg);
    if (!sensor_msg) {
      delete msg;
      continue;
    } else {
      sensor_msg->untaint();
    }

    PinholeImageMessage *rgb_msg =
        dynamic_cast<PinholeImageMessage *>(sensor_msg);
    if (!rgb_msg ||
        strcmp(rgb_msg->topic().c_str(), topic_name_image.c_str())) {
      delete rgb_msg;
      continue;
    }
    rgb_msg->untaint();

    // ds keypoint buffers
    std::vector<cv::KeyPoint> keypoints_FAST;
    std::vector<cv::KeyPoint> keypoints_ORB;
    std::vector<cv::KeyPoint> keypoints_SIFT;

    // bdc, get the current image reference
    cv::Mat image = rgb_msg->image();

    // bdc, get image filename and info
    const std::string image_filename = rgb_msg->binaryFilename();
    const int rows = image.rows;
    const int cols = image.cols;

    // bdc, create labels image
    const std::string labelled_image_filename =
        folder_labels + "/" + image_filename;
    // no-keypoints patches are labeled with NOK_
    const std::string nok_labelled_image_filename =
        folder_labels + "/" + "NOK_" + image_filename;
    cv::Mat labelled_image, labelled_color_image;
    labelled_image.create(rows, cols, CV_8UC1);
    labelled_image.setTo(cv::Scalar(0));
    labelled_color_image.create(rows, cols, CV_32FC3);
    labelled_color_image.setTo(cv::Scalar(0, 0, 0));

    // ds detect keypoints
    if(fast)
      detector_FAST->detect(image, keypoints_FAST);
    if(orb)
      detector_ORB->detect(image, keypoints_ORB);
    if(sift)
      detector_SIFT->detect(image, keypoints_SIFT);

    // ds descriptor based culling
    cv::Mat descriptors_BRIEF_from_FAST, descriptors_BRIEF_from_ORB,
        descriptors_BRIEF_from_SIFT;
    if(fast)
      descriptor_extractor->compute(image, keypoints_FAST, descriptors_BRIEF_from_FAST);
    if(orb)
      descriptor_extractor->compute(image, keypoints_ORB, descriptors_BRIEF_from_ORB);
    if(sift)
      descriptor_extractor->compute(image, keypoints_SIFT, descriptors_BRIEF_from_SIFT);

    // sort Fast feature by response
    if(fast)
      std::sort(keypoints_FAST.begin(), keypoints_FAST.end(),
		[](const cv::KeyPoint &a_, const cv::KeyPoint &b_) {
		  return a_.response > b_.response;
		});
    // sort ORB feature by response
    if(orb)
      std::sort(keypoints_ORB.begin(), keypoints_ORB.end(),
		[](const cv::KeyPoint &a_, const cv::KeyPoint &b_) {
		  return a_.response > b_.response;
		});
    // sort SIFT feature by response
    if(sift)
      std::sort(keypoints_SIFT.begin(), keypoints_SIFT.end(),
		[](const cv::KeyPoint &a_, const cv::KeyPoint &b_) {
		  return a_.response > b_.response;
		});

    
    // bdc, nasty check on channel number
    cv::Mat channel[3];

    // bdc channel splitting
    cv::split(image, channel);

    // bdc convert image to grayscale
    cv::Mat gray_image;
    if (!channel[0].empty() && !channel[1].empty() && !channel[2].empty())
      cv::cvtColor(image, gray_image, CV_BGR2GRAY);
    else if (!channel[0].empty())
      gray_image = channel[0];


    // extracted keypoint containers
    std::vector<cv::KeyPoint> good_FAST(0);
    std::vector<cv::KeyPoint> good_ORB(0);
    std::vector<cv::KeyPoint> good_SIFT(0);
    std::vector<cv::Point_<float>> bad_KeyPoints(0);

    
    // bdc, define number of target keypoints desired for each extractor
    const uint32_t target_number_of_FAST_keypoints = target_number_of_keypoints / number_of_different_keypoints;
    const uint32_t target_number_of_ORB_keypoints  = target_number_of_keypoints / number_of_different_keypoints;
    const uint32_t target_number_of_SIFT_keypoints = target_number_of_keypoints / number_of_different_keypoints;
    
    // bdc, extracting and saving (on disk) patches of best(in term of response) keypoints
    if(fast)
      extractGoodPatches(good_FAST, keypoints_FAST, patch_size, gray_image,
			 target_number_of_FAST_keypoints, image_filename,
			 labelled_image_filename, "_FAST", association_writer);
    if(orb)
      extractGoodPatches(good_ORB, keypoints_ORB, patch_size, gray_image,
			 target_number_of_ORB_keypoints, image_filename,
			 labelled_image_filename, "_ORB", association_writer);
    if(sift)
      extractGoodPatches(good_SIFT, keypoints_SIFT, patch_size, gray_image,
			 target_number_of_SIFT_keypoints, image_filename,
			 labelled_image_filename, "_SIFT", association_writer);
    
    // bdc, extracting and saving (on disk) patches of no features
    std::vector<std::vector<cv::KeyPoint> > mixture_of_keypoints;
    if(fast)
      mixture_of_keypoints.push_back(keypoints_FAST);
    if(orb)
      mixture_of_keypoints.push_back(keypoints_ORB);
    if(sift)
      mixture_of_keypoints.push_back(keypoints_SIFT);

    
    extractBadPatches(bad_KeyPoints, mixture_of_keypoints, patch_size,
                      gray_image, target_number_of_keypoints, image_filename,
                      nok_labelled_image_filename, association_writer);

    // ds info
    const uint32_t number_of_stored_keypoints =
        good_ORB.size() + good_FAST.size() + good_SIFT.size();
    const uint32_t number_of_stored_non_keypoints = bad_KeyPoints.size(); 
    std::printf(
        "%s|keypoints detected: %5lu|exported positives: %4u|negatives: %4u\n",
        image_filename.c_str(), keypoints_FAST.size() + keypoints_ORB.size() + keypoints_SIFT.size(),
        number_of_stored_keypoints, number_of_stored_non_keypoints);


    // bdc, draw Keypoints
    cv::Point_<float> offset(patch_size / 2, patch_size / 2);
    if(orb) { // blue for all point, light blue squares for good ones
      //drawKeyPoints(image, labelled_color_image, keypoints_ORB, blue);
      drawKeyPoints(image, labelled_color_image, good_ORB, CV_COLOR_LIGHTBLUE, patch_size);
    }
    if(fast) { // blue for all point, green squares for good ones
      //drawKeyPoints(image, labelled_color_image, keypoints_FAST, blue);
      drawKeyPoints(image, labelled_color_image, good_FAST, CV_COLOR_GREEN, patch_size);
    }
    if(sift) { // blue for all point, green squares for good ones
      //drawKeyPoints(image, labelled_color_image, keypoints_FAST, blue);
      drawKeyPoints(image, labelled_color_image, good_SIFT, CV_COLOR_ORANGE, patch_size);
    }
    drawPoints(image, labelled_color_image, bad_KeyPoints, CV_COLOR_RED, patch_size);

    // ds show image
    cv::imshow("current image", image);
    cv::imshow("labelled colors image", labelled_color_image);
    cv::waitKey(1);
    ++number_of_processed_messages;
  }

  if (number_of_processed_messages == 0) {
    std::printf("WARNING: no messages processed (check your command line "
                "parameters, e.g. camera topic)\n");
  }

#if CV_MAJOR_VERSION == 2
  delete detector_FAST;
  delete detector_ORB;
  delete detector_SIFT;
#endif

  association_writer.close();

  return 0;
}
