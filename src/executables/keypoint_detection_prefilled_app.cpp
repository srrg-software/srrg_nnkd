#include "srrg_system_utils/system_utils.h"
#include "srrg_messages/message_reader.h"
#include "srrg_messages/pinhole_image_message.h"

//#include <caffe/blob.hpp>
#include <caffe/caffe.hpp>
// #include <caffe/net.hpp>
// #include <caffe/util/io.hpp>
// #include <glog/logging.h>
// #include <google/protobuf/message.h>

#include <fstream>
#include <iostream>
#include <string.h>

#include <opencv2/core/version.hpp>
#include <opencv2/opencv.hpp>
#include <stdint.h>

#if CV_MAJOR_VERSION == 3
#include <opencv/cv.h>
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>
#endif

// using namespace google::protobuf;
// using google::protobuf::Message;
using namespace caffe;
using namespace std;
using namespace srrg_core;

//ds visualization only
#define CV_COLOR_BLUE cv::Scalar(255, 0, 0)
#define CV_COLOR_GREEN cv::Scalar(0, 255, 0)
#define CV_COLOR_RED cv::Scalar(0, 0, 255)

std::shared_ptr<caffe::Net<float>> gl_net;
cv::Size input_geometry_;
int num_channels_;
std::vector<cv::Mat> input_channels_;

void WrapBatchInputLayer(std::vector<std::vector<cv::Mat>> * input_batch) {
  Blob<float> *input_layer = gl_net->input_blobs()[0];
  
  int width = input_layer->width();
  int height = input_layer->height();
  int num = input_layer->num();
  float* input_data = input_layer->mutable_cpu_data();
  for ( int j = 0; j < num; j++){
    vector<cv::Mat> input_channels;
    for (int i = 0; i < input_layer->channels(); ++i){
      cv::Mat channel(height, width, CV_32FC1, input_data);
      input_channels.push_back(channel);
      input_data += width * height;
    }
    input_batch->push_back(vector<cv::Mat>(input_channels));
  }

}


void PreprocessBatch(const vector<cv::Mat> imgs,
		     std::vector< std::vector<cv::Mat> >* input_batch){
  for (int i = 0 ; i < imgs.size(); i++){
    const cv::Mat& img = imgs[i];

    std::vector<cv::Mat> *input_channels = &(input_batch->at(i));

    /* Convert the input image to the input image format of the network. */
    cv::Mat sample;
    if (img.channels() == 3 && num_channels_ == 1)
      cv::cvtColor(img, sample, CV_BGR2GRAY);
    else if (img.channels() == 4 && num_channels_ == 1)
      cv::cvtColor(img, sample, CV_BGRA2GRAY);
    else if (img.channels() == 4 && num_channels_ == 3)
      cv::cvtColor(img, sample, CV_BGRA2BGR);
    else if (img.channels() == 1 && num_channels_ == 3)
      cv::cvtColor(img, sample, CV_GRAY2BGR);
    else
      sample = img;
    
    cv::Mat sample_resized;
    if (sample.size() != input_geometry_)
      cv::resize(sample, sample_resized, input_geometry_);
    else
      sample_resized = sample;
    
    cv::Mat sample_float;
    if (num_channels_ == 3)
      sample_resized.convertTo(sample_float, CV_32FC3);
    else
      sample_resized.convertTo(sample_float, CV_32FC1);

    /* This operation will write the separate BGR planes directly to the
     * input layer of the network because it is wrapped by the cv::Mat
     * objects in input_channels. */
    cv::split(sample_float, *input_channels);
    
    //    std::cerr << input_channels->at(0).data << " " << gl_net->input_blobs()[0]->cpu_data() << std::endl;
    
    // CHECK(reinterpret_cast<float*>(input_channels->at(0).data)
    // 	  == gl_net->input_blobs()[0]->cpu_data())
    //   << "Input channels are not wrapping the input layer of the network.";
  }
}

/* netInit,
 * initialize the gl_net from the .prototxt
 * and .caffemodel files.
 */
void netInit(const string &proto_file, const string &trained_file,
             const bool gpu_mode) {
  // bdc, set CPU mode, static method
  if(gpu_mode)
    Caffe::set_mode(Caffe::GPU);
  else
    Caffe::set_mode(Caffe::CPU);
  // bdc, init the net
  gl_net.reset(new Net<float>(proto_file, TEST));
  gl_net->CopyTrainedLayersFrom(trained_file);

  if(gpu_mode)
    printf("\nGPU mode enabled\n\n");
  else
    printf("\nCPU mode enabled\n\n");
  
  input_geometry_ = cv::Size(30,30);
  // Blob<float> *input_layer = gl_net->input_blobs()[0];
  // input_geometry_ = cv::Size(input_layer->width(), input_layer->height());
  num_channels_ = 3;

}


std::vector<bool> binaryClassify(const std::vector<float>& net_output){
  double binary_start_time = srrg_core::getTime();
  std::vector<bool> net_answer;
  int size = net_output.size();
  int half_size = size/2;

  if(size%2 != 0)
    std::runtime_error("net output does not fit the number of classes!\n");

  net_answer.resize(half_size);
  

  for(int i=0, j=0; i < half_size && j < size; i++, j+=2){
    float false_label = net_output[j];
    float true_label = net_output[j+1];
   
    if(false_label > true_label)
      net_answer[i] = false;
    else
      net_answer[i] = true;

  }
  
  double binary_time = srrg_core::getTime() - binary_start_time;
  printf("Binary:\t%5.6f\n",binary_time);

  return net_answer;
  
}

std::vector<bool> predictBatch(const std::vector<cv::Mat> &imgs) {

  Blob<float> *input_layer = gl_net->input_blobs()[0];

  double reshape_time_start = srrg_core::getTime();
  input_layer->Reshape(imgs.size(), num_channels_,
                       input_geometry_.height,
                       input_geometry_.width);

  /* Forward dimension change to all layers. */
  gl_net->Reshape();
  double reshape_time = srrg_core::getTime() - reshape_time_start;
  printf("Reshape:\t%5.6f\n",reshape_time);

  
  std::vector< std::vector<cv::Mat> > input_batch;
  //input_batch.resize(imgs.size());
  
  double wrapInput_time_start = srrg_core::getTime();
  WrapBatchInputLayer(&input_batch);
  double wrapInput_time = srrg_core::getTime() - wrapInput_time_start;
  printf("wrapInput:\t%5.6f\n",wrapInput_time);

  double preprocess_time_start = srrg_core::getTime();  
  PreprocessBatch(imgs, &input_batch);
  double preprocess_time = srrg_core::getTime() - preprocess_time_start;
  printf("Preprocess:\t%5.6f\n",preprocess_time);

  double forward_time_start = srrg_core::getTime();  
  gl_net->Forward(); //ForwardPrefilled is deprecated
  double forward_time = srrg_core::getTime() - forward_time_start;
  printf("Forward:\t%5.6f\n",forward_time);
  
  /* Copy the output layer to a std::vector */
  Blob<float>* output_layer = gl_net->output_blobs()[0];
  const float* begin = output_layer->cpu_data();
  // std::cerr << "output lay channels: " << output_layer->channels() << std::endl;

  const float* end = begin + output_layer->channels()*imgs.size();
  std::vector<float> net_output = std::vector<float>(begin, end);

  return binaryClassify(net_output);
  
}


const char *banner[] = {
    "\n\nUsage:  keypoint_detection_gl_app -proto <prototxt_model> -model "
    "<trained_model> -t <rgb_topic> [Options] <dump_file.txt>\n",
    "Example:  keypoint_detection_gl_app -proto gl_net.prototxt -model "
    "gl_net_iter_10000.caffemodel -t /camera_left/image_raw 00.txt\n\n",
    "Options:\n", "------------------------------------------\n",
    "-proto <string>             network_model.prototxt",
    "-model <string>             network.caffemodel",
    "-t <string>                 RGB image topic name, as in the txtio file",
    "-gpu <flag>                 to use GPU mode, default false",
    "-verbose <flag>             google log info, default false",
    "-h                          this help\n", 0};

int32_t main(int32_t argc, char **argv) {

  //ds initialize google logging TODO blast this
  google::InitGoogleLogging(argv[0]);

  if (argc < 2) {
    printBanner(banner);
    return 1;
  }
 
  string rgb_topic = "/camera/rgb/image_raw";
  string proto_path = "";
  string model_path = "";
  string dump_file = "";
  bool gpu_mode = false;
  bool verbose = false;
  
  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-t")) {
      c++;
      rgb_topic = argv[c];
    } else if (!strcmp(argv[c], "-proto")) {
      c++;
      proto_path = argv[c];
    } else if (!strcmp(argv[c], "-model")) {
      c++;
      model_path = argv[c];
    } else if (!strcmp(argv[c], "-gpu")) {
      gpu_mode = true;
    } else if (!strcmp(argv[c], "-verbose")) {
      verbose = true;
    } else {
      dump_file = argv[c];
    }
    c++;
  }

  if (proto_path.empty() || model_path.empty() || dump_file.empty()) {
    cerr
        << "\nPlease provide a prototxt a caffemodel and a dump file. C'mon!\n";
    return 0;
  }

  cerr << "Caffe net Prototype:    " << proto_path << endl;
  cerr << "Trained model:          " << model_path << endl;
  cerr << "Dump File:              " << dump_file << endl;
  cerr << "Mode:                   ";
  (gpu_mode) ? cerr << "GPU" : cerr << "CPU";
  cerr << endl;

  // bdc, manage google logging verbosity
  if (!verbose) {
    google::SetCommandLineOption("GLOG_minloglevel", "2");
  }
  
  // bdc, timing stats
  double start_time = srrg_core::getTime();
  int number_of_frames_current_window = 0;
  const double measurement_interval_seconds = 2.0;
  
  // bdc, init the net
  netInit(proto_path, model_path, gpu_mode);

  // bdc, txtio reader
  MessageReader reader;
  reader.open(dump_file);

  // bdc, read messages
  BaseMessage *msg = 0;

  // bdc, vector of extracted keypoint
  std::vector<cv::KeyPoint> all_keypoints;
  std::vector<cv::KeyPoint> extracted_keypoints;
  std::vector<cv::Mat> grouped_patches;

  // bdc, reading from TXTIO
  while ((msg = reader.readMessage())) {
    BaseSensorMessage *sensor_msg = dynamic_cast<BaseSensorMessage *>(msg);
    if (!sensor_msg) {
      delete msg;
      continue;
    } else {
      sensor_msg->untaint();
    }

    PinholeImageMessage *rgb_msg =
        dynamic_cast<PinholeImageMessage *>(sensor_msg);
    if (!rgb_msg || strcmp(rgb_msg->topic().c_str(), rgb_topic.c_str())) {
      delete rgb_msg;
      continue;
    }
    rgb_msg->untaint();

    // bdc, get the image
    cv::Mat image_gray = rgb_msg->image();
    const int image_rows = image_gray.rows;
    const int image_cols = image_gray.cols;

    // bdc, sliding window on the image
    const int slw_rows = 30;      // pixel
    const int slw_cols = 30;      // pixel
    const int slw_row_init = 15;  // pixel
    const int slw_col_init = 15;  // pixel
    const int slw_step_size = 5; // pixel

    const int keypoint_size = 10;

    extracted_keypoints.clear();
    all_keypoints.clear();
    grouped_patches.clear();
    

    double sliding_window_start = srrg_core::getTime();
    
    // bdc, can be done better with row and col pointer. <--TODO
    for (int row = slw_row_init; row <= image_rows - slw_row_init;
         row += slw_step_size) {
      for (int col = slw_col_init; col <= image_cols - slw_col_init;
           col += slw_step_size) {
        // printf("[%d, %d] <--- [%d, %d]\n",image_rows, image_cols, row, col);
        // bdc, extracting patch
        cv::Mat patch = image_gray(cv::Rect(col - slw_col_init, row - slw_row_init,
					slw_cols, slw_rows));
	
	grouped_patches.push_back(patch);
        cv::KeyPoint new_keypoint(col, row, keypoint_size); // size fixed to 10
        all_keypoints.push_back(new_keypoint);

      }
    }

    double sliding_window_time = srrg_core::getTime() - sliding_window_start;
    printf("SlidingW:\t%5.6f\n",sliding_window_time);
    
    //once the patches have been grouped in a vector, we can call the net
    std::vector<bool> is_keypoint_vector = predictBatch(grouped_patches);
   
    std::vector<bool>::iterator keyp_it = is_keypoint_vector.begin();
    for(int i=0; keyp_it != is_keypoint_vector.end(); ++keyp_it, ++i) {
      if(*keyp_it)
	extracted_keypoints.push_back(all_keypoints[i]);
    }
    

    
    //ds get a colorized copy of the current image
    cv::Mat image_rgb;
    cv::cvtColor(image_gray, image_rgb, CV_GRAY2RGB);

    // bdc, show image and extracted keypoints   
    cv::Point2f offset(slw_rows/2.0, slw_cols/2.0);
    for (const cv::KeyPoint &keypoint: extracted_keypoints) {
      cv::circle(image_rgb, keypoint.pt, 2, CV_COLOR_GREEN, -1);
      cv::rectangle(image_rgb, keypoint.pt - offset, keypoint.pt + offset, CV_COLOR_BLUE);
    }
    cv::imshow("detection", image_rgb);
    cv::waitKey(1);

    ++number_of_frames_current_window;

    //bdc, show timing stats
    const double total_duration_seconds_current = srrg_core::getTime()-start_time;
    if(total_duration_seconds_current > measurement_interval_seconds) {
      printf("current fps: %5.2f\n", (double)number_of_frames_current_window/total_duration_seconds_current);
      number_of_frames_current_window = 0;
      start_time = srrg_core::getTime();
    }
    
    
  }

  return 0;
}
