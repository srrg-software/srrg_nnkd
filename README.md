# SRRG NNKD: Neural Network based Keypoint Detection

The aim of this project is to learn to extract, with a _convolutional neural network_, a feature that combines the main properties
of FAST, ORB and SIFT features.

## Required Prerequisites

* [Caffe](http://caffe.berkeleyvision.org/) - _deep neural network library_
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules) - _collection of cmake modules_
* [srrg_boss](https://gitlab.com/srrg-software/srrg_boss) - _serialization/deserialization library_
* [srrg_core](https://gitlab.com/srrg-software/srrg_core) - _core, i.e. algorithms and utilities_

## Optional Prerequisites

* [srrg_proslam/nnkd-branch](https://gitlab.com/srrg-software/srrg_proslam) - _stereo based SLAM pipeline (checkout **nnkd** branch)_


## Installation

The installation can be performed as a _ROS_ package, i.e.:

    cd <YOUR-ROS-SRC>
    git clone https://gitlab.com/srrg-software/srrg_nnkd
    cd ..
    catkin_make

## How to use it

You need a `txtio` dump.
Download one of Kitti sequence from [here](https://gitlab.com/srrg-software/srrg_proslam), or generate your own from a ROS bag with the message dumper in [srrg_core_ros](https://gitlab.com/srrg-software/srrg_core_ros).
All the app contained within this package provide an usage help by typing `-h` when launching them.

### Generate training data (Keypoint patches)

	mkdir training_keypoints
	rosrun srrg_nnkd keypoint_generation_app -t topic_camera -out training_keypoints/ -ass training_associations.txt dataset.txt

### Convert the dataset to lmdb format

The generated dataset can be converted in a single `lmdb` blob using the caffe tool `convert_imageset` e.g.

	${CAFFE_ROOT}/build/tools/convert_imageset training_keypoints/ training_associations.txt train_folder_lmdb

### Train the network

Modify the content of `glnet_solver.prototxt` and `glnet_train.prototxt` to fit with the data and models path, and run the training:

	${CAFFE_ROOT}/build/tools/caffe train --solver=glnet_solver.prototxt

It took ~80sec on a powerful machine.

### Load a pretrained model and detect keypoints

	rosrun srrg_nnkd keypoint_detection_gl_app -proto glnet_deploy.prototxt -model glnet_iter_9900.caffemodel -t /camera_left/image_raw 00.txt
	
If you compiled `Caffe` with the `USE_CUDA` flag activated, you can run the app in **GPU** mode by typing `-gpu`.

### Results

* Keypoint Detection

![](https://media.giphy.com/media/3og0IPpjiYtoJiiqli/giphy.gif)

* Detected Keypoint Tracking
 
![](https://media.giphy.com/media/3ohryf81psxDddaToQ/giphy.gif)

* Keypoint in a SLAM pipeline

![](https://media.giphy.com/media/3ohryBX8SFZiK0ySJi/giphy.gif)

### Slide

[Here](https://drive.google.com/file/d/0Bzl6DZ6clbE6WWZYTWlaRkFVOWc/view?usp=sharing) you can find the related presentation for the Introduction to Pattern Recognition course.

## Authors

* Bartolomeo Della Corte
* Dominik Schlegel
* Dario Albani
